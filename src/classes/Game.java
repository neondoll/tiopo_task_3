package classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
    private List<String[]> questions;
    private final List<Integer> currentQuestions;
    private final Player player;

    public Game() {
        setQuestions();

        currentQuestions = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            int randomQuestion;
            do {
                randomQuestion = (int) (Math.random() * questions.size());
            } while (currentQuestions.contains(randomQuestion));
            currentQuestions.add(randomQuestion);
        }

        player = new Player();
    }

    public List<Integer> getCurrentQuestions() {
        return currentQuestions;
    }

    public void run() {
        String[] answers = new String[]{"1", "2", "3", "4"};

        String answer;
        Scanner in = new Scanner(System.in);

        System.out.print("\n\n\nПривет! Это игра Кто хочет стать миллионером!\nКак тебя зовут?\nОтвет - ");
        player.setName(in.nextLine());
        System.out.print("\n\n\nОчень приятно, " + player.getName() + "\n");
        for (int i = 0; i < currentQuestions.size(); i++) {
            switch (i) {
                case 0 -> System.out.print("И так, первый вопрос на 100 рублей!\n\n\n");
                case 1 -> System.out.print("\n\n\n\nСледующий вопрос на 200 рублей!\n");
                case 2 -> System.out.print("\n\n\n\nСледующий вопрос на 300 рублей!\n");
                case 3 -> System.out.print("\n\n\n\nСледующий вопрос на 500 рублей!\n");
                case 4 -> System.out.print("\n\n\n\nСледующий вопрос на 1000 рублей!\n");
                case 5 -> System.out.print(" Это несгораемая сумма!\n\n\n\nСледующий вопрос на 2000 рублей!\n");
                case 6 -> System.out.print("\n\n\n\nСледующий вопрос на 4000 рублей!\n");
                case 7 -> System.out.print("\n\n\n\nСледующий вопрос на 8000 рублей!\n");
                case 8 -> System.out.print("\n\n\n\nСледующий вопрос на 16 000 рублей!\n");
                case 9 -> System.out.print("\n\n\n\nСледующий вопрос на 32 000 рублей!\n");
                case 10 -> System.out.print(" Это несгораемая сумма!\n\n\n\nСледующий вопрос на 64 000 рублей!\n");
                case 11 -> System.out.print("\n\n\n\nСледующий вопрос на 125 000 рублей!\n");
                case 12 -> System.out.print("\n\n\n\nСледующий вопрос на 250 000 рублей!\n");
                case 13 -> System.out.print("\n\n\n\nСледующий вопрос на 500 000 рублей!\n");
                case 14 -> System.out.print("\n\n\n\nИ итоговый вопрос на 1 000 000 рублей!\n");
            }
            System.out.print(questions.get(currentQuestions.get(i))[0] + "\n\n\nОтвет - ");
            answer = in.nextLine().replaceAll("\\s+", "");
            while (linearSearch(answers, answer) == -1) {
                System.out.print("\n\n\nПринимается ответ только 1, 2, 3 или 4\nОтвет - ");
                answer = in.nextLine().replaceAll("\\s+", "");
            }
            if (answer.equals(questions.get(currentQuestions.get(i))[1])) {
                player.setNumberCorrectAnswers(i + 1);
                if (i < currentQuestions.size() - 1) {
                    System.out.print("Это правильный ответ!\nТвой банк: " + player.getCurrentBank());
                } else {
                    System.out.print("Это правильный ответ!\nТы становишься победителем игры Кто хочет стать миллионером, забирая банк в " + player.getCurrentBank() + "!\n\n\n");
                }
            } else {
                System.out.print("К сожалению, ответ неправильный(((\nИгра окончена!\nТвой банк: " + player.getBank() + "\n\n\n");
                break;
            }
        }
    }

    private void setQuestions() {
        questions = Parser.run("/src/files/questions.txt");
    }

    private static int linearSearch(String[] arr, String elementToSearch) {

        for (int index = 0; index < arr.length; index++) {
            if (arr[index].equals(elementToSearch))
                return index;
        }
        return -1;
    }
}
