package classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Parser {
    public static List<String[]> run(String file_path) {
        File file = new File(new File("").getAbsolutePath() + file_path);
        List<String[]> lists = new ArrayList<>();
        Scanner scanner;
        if (file.exists() && !file.isDirectory()) {
            try {
                scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    String[] list = new String[2];
                    String nextLine = scanner.nextLine();
                    String[] data = nextLine.split(" / ");
                    for (int i = 0; i < data.length; i++) {
                        if (data[i].isEmpty()) continue;
                        list[i] = data[i].replace("&#9;", "\t").replace("&#10;", "\n");
                    }
                    lists.add(list);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return lists;
    }
}
