package classes;

public class Player {
    private String name;
    private int numberCorrectAnswers = 0;

    public String getName() {
        return name;
    }

    public String getBank() {
        return switch (numberCorrectAnswers) {
            default -> "0 руб.";
            case 5, 6, 7, 8, 9 -> "1000 руб.";
            case 10, 11, 12, 13, 14 -> "32 000 руб.";
            case 15 -> "1 000 000 руб.";
        };
    }

    public String getCurrentBank() {
        return switch (numberCorrectAnswers) {
            default -> "0 руб.";
            case 1 -> "100 руб.";
            case 2 -> "200 руб.";
            case 3 -> "300 руб.";
            case 4 -> "500 руб.";
            case 5 -> "1000 руб.";
            case 6 -> "2000 руб.";
            case 7 -> "4000 руб.";
            case 8 -> "8000 руб.";
            case 9 -> "16 000 руб.";
            case 10 -> "32 000 руб.";
            case 11 -> "64 000 руб.";
            case 12 -> "125 000 руб.";
            case 13 -> "250 000 руб.";
            case 14 -> "500 000 руб.";
            case 15 -> "1 000 000 руб.";
        };
    }

    public int getNumberCorrectAnswers() {
        return numberCorrectAnswers;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberCorrectAnswers(int numberCorrectAnswers) {
        this.numberCorrectAnswers = numberCorrectAnswers;
    }
}
