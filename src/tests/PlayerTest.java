package tests;

import classes.Player;
import org.junit.*;

import static org.junit.Assert.*;

public class PlayerTest {
    private Player player;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before PlayerTest.class");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("After PlayerTest.class");
    }

    @Before
    public void initTest() {
        player = new Player();
    }

    @After
    public void afterTest() {
        player = null;
    }

    @Test
    public void testSetAndGetName() {
        String name = "Александра";

        player.setName(name);
        assertEquals(name, player.getName());
    }

    @Test
    public void testSetAndGetNumberCorrectAnswers() {
        assertEquals(0, player.getNumberCorrectAnswers());
        player.setNumberCorrectAnswers(4);
        assertEquals(4, player.getNumberCorrectAnswers());
    }

    @Test
    public void testGetBank() {
        player.setNumberCorrectAnswers(3);
        assertEquals("0 руб.", player.getBank());
        player.setNumberCorrectAnswers(7);
        assertEquals("1000 руб.", player.getBank());
        player.setNumberCorrectAnswers(14);
        assertEquals("32 000 руб.", player.getBank());
        player.setNumberCorrectAnswers(15);
        assertEquals("1 000 000 руб.", player.getBank());
    }

    @Test
    public void testGetCurrentBank() {
        String[] currentBank = {
                "0 руб.", "100 руб.", "200 руб.", "300 руб.", "500 руб.", "1000 руб.", "2000 руб.", "4000 руб.",
                "8000 руб.", "16 000 руб.", "32 000 руб.", "64 000 руб.", "125 000 руб.", "250 000 руб.",
                "500 000 руб.", "1 000 000 руб."
        };
        int random = (int) (Math.random() * currentBank.length);
        player.setNumberCorrectAnswers(random);
        assertEquals(currentBank[random], player.getCurrentBank());
    }
}
