package tests;

import classes.Game;
import org.junit.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GameTest {
    private Game game;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before GameTest.class");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("After GameTest.class");
    }

    @Before
    public void initTest() throws FileNotFoundException {
        game = new Game();
    }

    @After
    public void afterTest() {
        game = null;
    }

    @Test
    public void testGetNumberCurrentQuestions() {
        assertEquals(15, game.getCurrentQuestions().size());
    }

    @Test
    public void testCurrentQuestionsAreUnique() {
        List<Integer> currentQuestions = game.getCurrentQuestions();
        List<Integer> uniqueCurrentQuestions = new ArrayList<>();
        for (int x : currentQuestions) {
            if (!uniqueCurrentQuestions.contains(x)) {
                uniqueCurrentQuestions.add(x);
            }
        }
        assertEquals(currentQuestions.size(), uniqueCurrentQuestions.size());
    }
}
